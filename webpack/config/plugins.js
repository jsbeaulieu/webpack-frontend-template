const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const { removeEmpty } = require('webpack-config-utils')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const { env } = require('../config')

module.exports = function (distPath) {
  const { ifDevelopment, ifProduction } = require('../helpers')(env)

  return removeEmpty([
    // Common plugins
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': `"${env}"`
      }
    }),
    new ExtractTextPlugin(ifProduction(
      `${distPath}/[name].min.css`,
      `${distPath}/[name].css`)
    ),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      filename: ifProduction(
        `${distPath}/vendors.min.js`,
        `${distPath}/vendors.js`
      ),
      minChunks: Infinity
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'vendors',
      filename: ifProduction(
        `${distPath}/vendors.min.js`,
        `${distPath}/vendors.js`
      ),
      minChunks (module, count) {
        return module.context && module.context.indexOf('node_modules') >= 0
      }
    }),

    // Development only
    ...ifDevelopment([
      new webpack.HotModuleReplacementPlugin(),
      new webpack.NamedModulesPlugin(),
      new webpack.NoEmitOnErrorsPlugin()
    ], []),

    // Production only
    ...ifProduction([
      new webpack.optimize.UglifyJsPlugin({ compressor: { warnings: false } }),
      new OptimizeCssAssetsPlugin({
        assetNameRegExp: /\.min\.css$/,
        cssProcessorOptions: { discardComments: { removeAll: true } }
      })
    ], [])

  ])
}
